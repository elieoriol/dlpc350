Adapted from the work of Alexander Tomlinson (Sivyer Lab) suited to what we do in the Behnia Lab. A lot of the code has been kept as written in pycrafter4500. Below is an adapted readme for our version.

# dlpc350

This is an unofficial API to communicate with the DLPC 350 controller chip (e.g. Texas Instruments Lightcrafter 4500).

Code adapted from pycrafter4500 (https://github.com/SivyerLab/pyCrafter4500) itself adapted from https://github.com/csi-dcsc/Pycrafter6500

TI DLPC 350 documentation can be found at: http://www.ti.com/lit/ug/dlpu010f/dlpu010f.pdf

To connect to the LCR4500, the libusb-win32 driver is required. The recommended way to do this is
with [Zadig](http://zadig.akeo.ie/). The pyusb package is also required.

You can connect to any projector that has a DLPC 350 controller chip. You would need its Vendor ID and Product ID.

## Install

You can install it locally

```bash
git clone git@gitlab.com:elieo/dlpc350.git
cd dlpc350
pip install -e .
```


## Usage

Import and connects to a LCR 4500 over USB if one is connected. Its Vendor ID and Product ID are 0x0451 and 0x6401

```python
import dlpc350
lcr = dlpc350.connect(vid=0x0451, pid=0x6401)
```

To connect a second UV LCR 4500

```python
import dlpc350
lcr = dlpc350.connect()
lcr_uv = dlpc350.connect(vid=0x0451, pid=0x6401, uv=True)
```

Turn on or off

```python
lcr.power_up()
lcr.power_down()
```

Putting into fast standby (solid black internal test pattern) and fast waking up

```python
lcr.disable()
lcr.enable()
```

To set the projector to RGB, monochrome Red, monochrome Green, monochrome Blue (that can work at 180 Hz with framepacking)

```python
lcr.color_mode(color='rgb')
lcr.color_mode(color='red')
lcr.color_mode(color='green')
lcr.color_mode(color='blue')
```

To get current color mode (returns 'rgb', 'red', 'green', 'blue' or '' if none of the previous)

```python
color_mode = lcr.current_color_mode()
```

To disconnect the projector and free the USB connection (it will delete the object instance)

```python
lcr.disconnect()
```

There are other commands already implemented in the dlpc350 class.
If you wish to send other commands, this can be done using the class as well to write your own. See source documentation for further details.

```python
from dlpc350 import dlpc350, connect
from dlpc350 import bits_to_bytes, conv_len

lcr = dlpc350.connect()
lcr.command('w', 0x00, CMD2, CMD3, payload)
```
