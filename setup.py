from distutils.core import setup

setup(
    name = 'dlpc350',
    packages = ['dlpc350'], # this must be the same as the name above
    version = '1.0',
    description = 'A python interface to communicate over USB with the TI Lightcrafter 4500',
    author = 'Elie Oriol',
    author_email = 'elie.oriol@polytechnique.edu',
    url = 'https://gitlab.com/elieo/dlpc350',
    keywords = 'lightcrafter 4500 dlpc 350 projector texas',
    install_requires=[
        'pyusb'
    ],
    license='MIT',
)